<?php

//ЗАДАЧА
//
//Давайте попробуем написать игру-угадывалку, состоящую из нескольких файлов.
//
//Файл index.php у нас будет главным, на нем все и будет происходить.
//В него подключим файл form.php – в нем расположим форму с одним текстовым полем и кнопкой «угадать».
//В поле мы вводим цифру и отправляем на index.php.
//Там проверяем что пришли данные из формы и если они пришли, то генерируем число от 5 до 8 и
// сравниваем с пришедшим числом из формы.
//
//Если число пришло меньше 5 – пишем что число маленькое.
//Если больше 8 – то пишем что число большое.
//Если мы угадали число – выводи на экран надпись «вы угадали» и ссылку на пустую форму с надписью «Играть еще».
//Если не угадали, но число в нужном диапазоне – выводим опять форму с надписью «попробуйте еще»

if (isset($_POST['number']) && $_POST['number'] !== "") {
    var_dump($_POST['number']);
    $number = rand(5, 8);
    if ($_POST['number'] < 5) {
        echo 'Число маленькое';
    } elseif ($_POST['number'] > 8) {
        echo 'Число большое';
    } elseif ($_POST['number'] == $number) {
        echo "Вы угадали! <a href='form.php'>Играть еще!</a>";
    } else {
        echo "Попробуйте еще.</br>";
        require_once "form.php";
    }
} else {
    echo "Некорректные данные";
}
