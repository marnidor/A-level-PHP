<?php

//1. Дан массив
//['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
//Развернуть этот массив в обратном направлении
$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$count = 0;
foreach ($arr as $value) {
    $count++;
}

$arrChange1 = [];
$arrChange2 = [];
$arrChange3 = [];
$arrChange4 = [];
for ($i = 1; $i <= $count; $i++) {
    $arrChange1[$i] = $arr[$count - $i];
}
print_r($arrChange1);
echo "</br>";

$i = 0;
while ($i < $count) {
    $i++;
    $arrChange2[$i] = $arr[$count - $i];
}
print_r($arrChange2);
echo "</br>";

$i = 1;
do {
    $arrChange3[$i] = $arr[$count - $i];
    $i++;
} while ($i <= $count);
print_r($arrChange3);
echo "</br>";

$i = 0;
foreach ($arr as $key => $value) {
    $arrChange1[$i] = $arr[$count - $i];
    $i++;
}
print_r($arrChange3);

echo "<hr>";


//2. Дан массив
//[44, 12, 11, 7, 1, 99, 43, 5, 69]
//Развернуть этот массив в обратном направлении
$arr2 = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$count = 0;
foreach ($arr2 as $value) {
    $count++;
}

$arrChange1 = [];
$arrChange2 = [];
$arrChange3 = [];
$arrChange4 = [];

for ($i = 1; $i <= $count; $i++) {
    $arrChange1[$i] = $arr2[$count - $i];
}
print_r($arrChange1);
echo "</br>";

$i = 0;
while ($i < $count) {
    $i++;
    $arrChange2[$i] = $arr2[$count - $i];
}
print_r($arrChange2);
echo "</br>";

$i = 1;
do {
    $arrChange3[$i] = $arr2[$count - $i];
    $i++;
} while ($i <= $count);
print_r($arrChange3);
echo "</br>";


$i = 0;
foreach ($arr as $key => $value) {
    $arrChange1[$i] = $arr2[$count - $i];
    $i++;
}
print_r($arrChange3);
echo "<hr>";

//3. Дана строка
//let str = 'Hi I am ALex'
//развенуть строку в обратном направлении.

$str = 'Hi I am Alex';
$len = 0;
$i = 0;
while (isset($str[$i])) {
    $len++;
    $i++;
}
echo $len . "</br>";
$str1 = '';
$str2 = '';
$str3 = '';
$str4 = '';

for ($i = 0; $i <= $len; $i++) {
    $str1[$i] = $str[$len - $i];
}
echo $str1 . "</br>";

$i = 0;
while ($i < $len) {
    $i++;
    $str2[$i] = $str[$len - $i];
}
echo $str2 . "</br>";

$i = 1;
do {
    $str3[$i] = $str[$len - $i];
    $i++;
} while ($i <= $len);
echo $str3;
echo "<hr>";


//4. Дана строка. готовую функцию toUpperCase() or tolowercase()
//str = 'Hi I am ALex'
//сделать ее с маленьких букв

$str = 'Hi I am ALex';
$str1 = '';
$str2 = '';
$str3 = '';
$str4 = '';

for ($i = 0; $i < $len; $i++) {
    $str1[$i] = strtolower($str[$i]);
}
echo $str1 . "</br>";

$i = 0;
while ($i < $len + 1) {
    $str2[$i] = strtolower($str[$i]);
    $i++;
}
echo $str2 . "</br>";

$i = 0;
do {
    $str3[$i] = strtolower($str[$i]);
    $i++;
} while ($i <= $len);
echo $str3;
echo "<hr>";

//5. Дана строка
//let str = 'Hi I am ALex'
//сделать все буквы большие
$str = 'Hi I am ALex';
$str1 = '';
$str2 = '';
$str3 = '';
$str4 = '';

for ($i = 0; $i < $len; $i++) {
    $str1[$i] = strtoupper($str[$i]);
}
echo $str1 . "</br>";

$i = 0;
while ($i < $len + 1) {
    $str2[$i] = strtoupper($str[$i]);
    $i++;
}
echo $str2 . "</br>";

$i = 0;
do {
    $str3[$i] = strtoupper($str[$i]);
    $i++;
} while ($i <= $len);
echo $str3;
echo "<hr>";

//7. Дан массив
//['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
//сделать все буквы с маленькой

$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$count = 0;
foreach ($arr as $value) {
    $count++;
}

$arrChange1 = [];
$arrChange2 = [];
$arrChange3 = [];
$arrChange4 = [];

for ($i = 0; $i < $count; $i++) {
    $arrChange1[$i] = strtolower($arr[$i]);
}
print_r($arrChange1);
echo "</br>";

$i = 0;
while ($i < $count) {
    $arrChange2[$i] = strtolower($arr[$i]);
    $i++;
}
print_r($arrChange2);
echo "</br>";


$i = 0;
do {
    $arrChange3[$i] = strtolower($arr[$i]);
    $i++;
} while ($i < $count);
print_r($arrChange3);
echo "</br>";

$i = 0;
foreach ($arr as $key => $value) {
    $arrChange4[$key] = strtolower($value);
}
print_r($arrChange4);
echo "<hr>";

//8. Дан массив
//['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
//сделать все буквы с большой

$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$count = 0;
foreach ($arr as $value) {
    $count++;
}

$arrChange1 = [];
$arrChange2 = [];
$arrChange3 = [];
$arrChange4 = [];

for ($i = 0; $i < $count; $i++) {
    $arrChange1[$i] = strtoupper($arr[$i]);
}
print_r($arrChange1);
echo "</br>";

$i = 0;
while ($i < $count) {
    $arrChange2[$i] = strtoupper($arr[$i]);
    $i++;
}
print_r($arrChange2);
echo "</br>";


$i = 0;
do {
    $arrChange3[$i] = strtoupper($arr[$i]);
    $i++;
} while ($i < $count);
print_r($arrChange3);
echo "</br>";

$i = 0;
foreach ($arr as $key => $value) {
    $arrChange4[$key] = strtoupper($value);
}
print_r($arrChange4);
echo "<hr>";

//9. Дано число
// num = 1234678
//развернуть ее в обратном направлении\

$num = 1234678;
$str = (string) $num;
$len = 0;
$i = 0;
while (isset($str[$i])) {
    $len++;
    $i++;
}
echo $len . "</br>";
$str1 = '';
$str2 = '';
$str3 = '';
$str4 = '';

for ($i = 0; $i <= $len; $i++) {
    $str1[$i] = $str[$len - $i];
}
echo $str1 . "</br>";

$i = 0;
while ($i < $len) {
    $i++;
    $str2[$i] = $str[$len - $i];
}
echo $str2 . "</br>";

$i = 1;
do {
    $str3[$i] = $str[$len - $i];
    $i++;
} while ($i <= $len);
echo $str3;
echo "<hr>";

//10. Дан массив
//[44, 12, 11, 7, 1, 99, 43, 5, 69]
//отсортируй его в порядке убывания

$arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$count = 0;
foreach ($arr as $value) {
    $count++;
}
for ($i = 0; $i < $count; $i++) {
    for ($j = 0; $j < $count - 1; $j++) {
        if ($arr[$j] <= $arr[$j + 1]) {
            $temp = $arr[$j];
            $arr[$j] = $arr[$j + 1];
            $arr[$j + 1] = $temp;
        }
    }
}
print_r($arr);
echo "</br>";

$arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$i = 0;
while ($i < $count) {
    $j = 0;
    while ($j < $count - 1) {
        if ($arr[$j] <= $arr[$j + 1]) {
            $temp = $arr[$j];
            $arr[$j] = $arr[$j + 1];
            $arr[$j + 1] = $temp;
        }
        $j++;
    }
    $i++;
}
print_r($arr);
echo "</br>";


$arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$i = 0;
do {
    $j = 0;
    do {
        if ($arr[$j] <= $arr[$j + 1]) {
            $temp = $arr[$j];
            $arr[$j] = $arr[$j + 1];
            $arr[$j + 1] = $temp;
        }
        $j++;
    } while ($j < $count - 1);
    $i++;
} while ($i <= $count);

print_r($arr);
echo "</br>";

$arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
foreach ($arr as $key => $value) {
    for ($j = 0; $j < $count - 1; $j++) {
        if ($arr[$j] <= $arr[$j + 1]) {
            $temp = $arr[$j];
            $arr[$j] = $arr[$j + 1];
            $arr[$j + 1] = $temp;
        }
    }
}
print_r($arr);
echo "</br>";
