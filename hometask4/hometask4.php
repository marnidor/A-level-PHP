<?php

for ($i = 1; $i <= 100; $i++) {
    $flag = 0;
    for ($j = 2; $j < $i; $j++) {
        if ($i % $j == 0) {
            $flag++;
        }
    }
    if ($flag == 0) {
        echo $i . " ";
    }
}
echo "<br><br>";

echo "<br><br>";
//2) Сгенерируйте 100 раз новое число и выведите
//на экран количество четных чисел из этих 100.
$count = 0;
for ($i = 0; $i <= 100; $i++) {
    $num = random_int(1, 100);
    echo $num . ' ';
    if ($num % 2 == 0) {
        $count += 1;
    }
}
echo '</br>';
echo "Count = $count" . '</br>' . '</br>';

//Сгенерируйте 100 раз число от 1 до 5 и выведите на экран сколько
//раз сгенерировались эти числа (1, 2, 3, 4 и 5).
$count1 = 0;
$count2 = 0;
$count3 = 0;
$count4 = 0;
$count5 = 0;
for ($i = 0; $i <= 10; $i++) {
    $num = random_int(1, 5);
    echo $num . ' ';
    if ($num == 1) {
        $count1 += 1;
    } elseif ($num == 2) {
        $count2 += 1;
    } elseif ($num == 3) {
        $count3 += 1;
    } elseif ($num == 4) {
        $count4 += 1;
    } elseif ($num == 5) {
        $count5 += 1;
    }
}
echo '</br>';
echo "Number 1 - $count1 times" . '</br>';
echo "Number 2 - $count2 times" . '</br>';
echo "Number 3 - $count3 times" . '</br>';
echo "Number 4 - $count4 times" . '</br>';
echo "Number 5 - $count5 times" . '</br>' . '</br>';

//4) Используя условия и циклы сделать таблицу в 5 колонок и 3 строки
//(5x3), отметить разными цветами часть ячеек.
$a = 0;
$b = 0;
$c = 0;
echo "<table border='1'";
for ($a = 0; $a <= 200; $a += 100) {
    echo "<tr>";
    for ($b = 0; $b <= 200; $b += 50) {
        for ($c = 0; $c <= 200; $c += 500) {
            echo "<td style=background-color:rgb($a,$b,$c);>Color</td>";
        }
    }
    echo "</tr>";
}
echo "</table>" . '</br>' . '</br>';

//1. В переменной month лежит какое-то число из интервала от 1 до 12. Определите в какую
//пору года попадает этот месяц (зима, лето, весна, осень).
$month = random_int(1, 12);
if ($month == 1 || $month == 1 || $month == 12) {
    echo "It's winter." . '</br>' . '</br>';
} elseif ($month == 3 || $month == 4 || $month == 5) {
    echo "It's spring." . '</br>' . '</br>';
} elseif ($month == 6 || $month == 7 || $month == 8) {
    echo "It's summer." . '</br>' . '</br>';
} elseif ($month == 9 || $month == 10 || $month == 11) {
    echo "It's autmn." . '</br>' . '</br>';
}

//Дана строка, состоящая из символов, например, 'abcde'.
//Проверьте, что первым символом этой строки является буква 'a'.
//Если это так - выведите 'да', в противном случае выведите 'нет'.
$str = 'abcde';
if ($str[0] == 'a') {
    echo 'Yes' . '</br>' . '</br>';
} else {
    echo 'No' . '</br>' . '</br>';
}

//Дана строка с цифрами, например, '12345'.
//Проверьте, что первым символом этой строки является цифра 1, 2 или 3.
//Если это так - выведите 'да', в противном случае выведите 'нет'.
$str = '12345';
if ($str[0] == '1' || $str[1] == '2' || $str[1] == '3') {
    echo 'Yes' . '</br>' . '</br>';
} else {
    echo 'No' . '</br>' . '</br>';
}

//Если переменная test равна true, то выведите 'Верно', иначе выведите 'Неверно'.
//Проверьте работу скрипта при test, равном true, false.
//Напишите два варианта скрипта - тернарка и if else.
$test1 = true;
$test2 = false;
if ($test1 == true || $test2 == false) {
    echo 'True' . '</br>' . '</br>';
} else {
    echo 'False' . '</br>' . '</br>';
}
echo ($test1 == true || $test2 == false) ? ('True' . '</br>' . '</br>') : ('False' . '</br>' . '</br>');

//Дано Два массива рус и англ ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс']
//Если переменная lang = ru вывести массив на русском языке, а если en то
//вывести на английском языке. Сделат через if else и через тернарку.

$arrayEng = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
$arrayRus = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'];
$lang = 'ru';
if ($lang == 'ru') {
    echo $arrayEng . '</br>' . '</br>';
} else {
    echo $arrayRus . '</br>' . '</br>';
}
print_r(($lang == 'ru') ? ($arrayEng) : ($arrayRus));
echo '</br>' . '</br>';

//В переменной cloсk лежит число от 0 до 59 – это минуты.
//Определите в какую четверть часа попадает это число
//(в первую, вторую, третью или четвертую). тернарка и if else.
$clock = rand(1, 59);
echo $clock . '</br>';
if ($clock >= 1 && $clock <= 15) {
    echo 'The first' . '</br>' . '</br>';
} elseif ($clock >= 16 && $clock <= 30) {
    echo 'The second' . '</br>' . '</br>';
} elseif ($clock >= 31 && $clock <= 45) {
    echo 'The third' . '</br>' . '</br>';
} elseif ($clock >= 46 && $clock <= 59) {
    echo 'The fourth' . '</br>' . '</br>';
}

$part = ($clock >= 1 && $clock <= 15) ? 'The first' : (
($clock >= 16 && $clock <= 30) ? 'The second' : (
($clock >= 31 && $clock <= 45) ? 'The third' : (
($clock >= 46 && $clock <= 59) ? 'The fourth' : ''
)));
echo $part;
echo "<hr>";

//Все задания делаем
//while
//do while
//for
//foreach


//1. Дан массив
//['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
//Развернуть этот массив в обратном направлении
$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$count = 0;
foreach ($arr as $value) {
    $count++;
}

$arrChange1 = [];
$arrChange2 = [];
$arrChange3 = [];
$arrChange4 = [];
for ($i = 1; $i <= $count; $i++) {
    $arrChange1[$i] = $arr[$count - $i];
}
print_r($arrChange1);
echo "</br>";

$i = 0;
while ($i < $count) {
    $i++;
    $arrChange2[$i] = $arr[$count - $i];
}
print_r($arrChange2);

do {
    $i = 0;
    $arrChange3[$i] = $arr[$count - $i];
    $i++;
} while ($i < $count);
print_r($arrChange3);
