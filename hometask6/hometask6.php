<?php

/**
 * @param array $arr
 * @return void
 */
function dd(array $arr): void
{
    echo "<pre>";
    print_r($arr);
    echo "</pre>";
}

echo "<hr>";


//Найти минимальное и максимальное среди 3 чисел
/**
 * @param int $num1
 * @param int $num2
 * @param int $num3
 * @return int
 */
$minMax = function (int $num1, int $num2, int $num3): int {
    $arr = [$num1, $num2, $num3];
    $min = $arr[0];
    for ($i = 0; $i <= 2; $i++) {
        if ($arr[$i] <= $min) {
            $min = $arr[$i];
        }
    }
    return $min;
};
echo $minMax($num1 = rand(1, 10), $num2 = rand(1, 10), $num3 = rand(1, 10)) . "</br>";
echo $num1 . ' ' . $num2 . ' ' . $num3 . "</br>";
echo "</br>";

//стрелочная
/**
 * @param int $num1
 * @param int $num2
 * @param int $num3
 * @return int
 */
function calcMin(int $num1, int $num2, int $num3): int
{
    $arr = [$num1, $num2, $num3];
    $min = $arr[0];
    for ($i = 0; $i <= 2; $i++) {
        if ($arr[$i] <= $min) {
            $min = $arr[$i];
        }
    }
    return $min;
}

$minMax2 = fn($num1 = 5, $num2 = 4, $num3 = 9) => calcMin($num1, $num2, $num3);
echo $minMax2();
echo "<hr>";


//Найти площадь
//function expression
/**
 * @param int $len
 * @param int $wid
 * @return int
 */
$calcSquare1 = function (int $len, int $wid): int {
    return $len * $wid;
};
echo $calcSquare1(5, 5) . "</br>";

//стрелочная
/**
 * @param int $len
 * @param int $wid
 * @return int
 */
$calcSquare1 = fn(int $len, int $wid): int => $len * $wid;
echo $calcSquare1(7, 7) . "</br>";

//Теорема Пифагора
//function expression
/**
 * @param int $leg1
 * @param int $Leg2
 * @return int
 */
$pythagor1 = function (int $leg1, int $Leg2): int {
    return sqrt($leg1 ** 2 + $Leg2 ** 2);
};
echo $pythagor1(12, 5) . "</br>";

//стрелочная
/**
 * @param int $leg1
 * @param int $Leg2
 * @return float
 */
$pythagor2 = fn(int $leg1, int $Leg2): float => sqrt($leg1 ** 2 + $Leg2 ** 2);
echo $pythagor2(5, 12) . "</br>";

//Найти периметр
//function expression
/**
 * @param int $len1
 * @param int $Len2
 * @param int $len3
 * @return int
 */
$perimetr1 = function (int $len1, int $Len2, int $len3): int {
    return $len1 + $Len2 + $len3;
};
echo $perimetr1(12, 5, 17) . "</br>";


//стрелочная
/**
 * @param int $len1
 * @param int $Len2
 * @param int $len3
 * @return int
 */
$perimetr2 = fn(int $len1, int $Len2, int $len3): int => $len1 + $Len2 + $len3;
echo $perimetr2(5, 12, 10) . "</br>";

//Найти дискриминант
//function expression
/**
 * @param int $a
 * @param int $b
 * @param int $c
 * @return float
 */
$discriminant1 = function (int $a, int $b, int $c): float {
    return $b ** 2 - 4 * $a * $c;
};
echo $discriminant1(1, 5, 2) . "</br>";


//стрелочная
$discriminant2 = fn(int $a, int $b, int $c): int => $b ** 2 - 4 * $a * $c;
echo $discriminant2(1, 5, 2) . "</br>" . "</br>";


//Создать только четные числа до 100
//function expression
/**
 * @return void
 */
$even1 = function (): void {
    for ($i = 0; $i < 100; $i++) {
        if ($i % 2 == 0) {
            echo $i . ' ';
        }
    }
};
$even1();
echo "</br>" . "</br>";


//стрелочная
/**
 * @return void
 */
function calcEven(): void
{
    for ($i = 0; $i < 100; $i++) {
        if ($i % 2 == 0) {
            echo $i . ' ';
        }
    }
}

$even2 = fn() => calcEven();
$even2();
echo "</br>" . "</br>";

//Создать нечетные числа до 100
//function expression
/**
 * @return void
 */
$odd1 = function (): void {
    for ($i = 0; $i < 100; $i++) {
        if ($i % 2 == 1) {
            echo $i . ' ';
        }
    }
};
$odd1();
echo "</br>" . "</br>";


//стрелочная
/**
 * @return void
 */
function calcOdd(): void
{
    for ($i = 0; $i < 100; $i++) {
        if ($i % 2 == 1) {
            echo $i . ' ';
        }
    }
}

$odd2 = fn() => calcOdd();
$odd2();
echo "</br>" . "</br>";

//Определите, есть ли в массиве повторяющиеся элементы.
//function expression
/**
 * @param array $arr
 * @return int
 */
$isRepeat1 = function (array $arr): int {
    $is = 0;
    $count = count($arr);
    for ($i = 0; $i < $count; $i++) {
        for ($j = 0; $j < $count; $j++) {
            if ($arr[$i] == $arr[$j] && $i != $j) {
                $is = 1;
                break;
            } else {
                $is = 0;
            }
        }
    }
    return $is;
};
$array1 = [1, 2, 4, 6, 23, 56, 1, 8, 23];
echo $isRepeat1($array1);
echo "</br>" . "</br>";

//стрелочная
/**
 * @param array $arr
 * @return int
 */
function isRepeate(array $arr): int
{
    $is = 0;
    $count = count($arr);
    for ($i = 0; $i < $count; $i++) {
        for ($j = 0; $j < $count; $j++) {
            if ($arr[$i] == $arr[$j] && $i != $j) {
                $is = 1;
                break;
            } else {
                $is = 0;
            }
        }
    }
    return $is;
}

$isRepeat2 = fn($array = [1, 2, 4, 6, 23, 56, 1, 8, 23]) => isRepeate($array);
echo $isRepeat2();


//Сортировка из прошлого задания
//function expression
/**
 * @param $arr
 * @param string $sortType
 * @return array
 */
$sortArr = function (array $arr, string $sortType = 'asc'): array {
    $len = count($arr);
    if ($sortType !== 'asc' || $sortType !== 'desc') {
        $sortType = 'asc';
    }
    for ($i = 0; $i < $len; $i++) {
        for ($j = 0; $j < $len - 1; $j++) {
            if (
                ($arr[$j] >= $arr[$j + 1] && $sortType == 'asc') ||
                ($arr[$j] <= $arr[$j + 1] && $sortType == 'desc')
            ) {
                $temp = $arr[$j];
                $arr[$j] = $arr[$j + 1];
                $arr[$j + 1] = $temp;
            }
        }
    }
    return $arr;
};

$array = [2, 4, 12, 7, 45, 100, 34, 1];
print_r($sortArr($array));

//стрелочная
/**
 * @param array $arr
 * @param string $sortType
 * @return array
 */
function sortArray(array $arr, string $sortType = 'asc'): array
{
    $len = count($arr);
    if ($sortType !== 'asc' || $sortType !== 'desc') {
        $sortType = 'asc';
    }
    for ($i = 0; $i < $len; $i++) {
        for ($j = 0; $j < $len - 1; $j++) {
            if (
                ($arr[$j] >= $arr[$j + 1] && $sortType == 'asc') ||
                ($arr[$j] <= $arr[$j + 1] && $sortType == 'desc')
            ) {
                $temp = $arr[$j];
                $arr[$j] = $arr[$j + 1];
                $arr[$j + 1] = $temp;
            }
        }
    }
    return $arr;
}

$sortArr2 = fn($arr = [2, 4, 12, 7, 45, 100, 34, 1]) => sortArray($arr);
print_r($sortArr2());

echo "<hr>";
