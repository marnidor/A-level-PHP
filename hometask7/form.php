<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
    <title>HOMETASK</title>
</head>
<body>

<div class=“container">

    <form action="data.php" method="post">
        <div class="form-row">
            <div class="form-group col-md-6">
                <input type="email" class="form-control" name="inputEmail" placeholder="Enter email" required>
                <br><br>
            </div>
            <div class="form-group col-md-6">
                <input type="password" class="form-control" name="inputPassword" placeholder="Enter password" required>
            </div>
            <br><br>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="inputAddress" placeholder="Enter address" required>
        </div>
        <br><br>
        <div class="form-row">
            <div class="form-group col-md-6">
                <input type="text" class="form-control badge badge-pill badge-light" name="inputCity" placeholder="Enter city" required>
            </div>
            <br><br>
            <div class="form-group col-md-2">
                <input type="number" class="form-control" name="inputIndex" placeholder="Enter index" required>
            </div>
        </div>
        <br>
        <div class="form-group">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" required> Check me out
            </div>
        </div>
        <br>
        <button type="submit" class="btn btn-outline-primary">Sign in</button>
    </form>
</div>


</body>
</html>