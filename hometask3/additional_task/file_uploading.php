<?php

const ALLOWED_TYPES = ['png', 'pdf', 'doc', 'docx', 'xls', 'jpg', 'jpeg'];
if (isset($_FILES["userfile"])) {
    if (is_uploaded_file($_FILES['userfile']['tmp_name'])) {
        $type = explode(".", $_FILES["userfile"]["name"])[1];
        if (in_array($type, ALLOWED_TYPES)) {
            $filename = basename($_FILES['userfile']['name']);
            $uploaddir = '/var/www/html/public';
            $uploadfile = $uploaddir . $filename;
            move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile);
            echo "<img src='../$uploadfile' alt='$filename' title='$filename' />";
        } else {
            echo 'Error: unsupported type';
        }
    } else {
        echo "Error: empty file.";
    }
} else {
    echo '
<form enctype="multipart/form-data" method="post">
  Send this file: <input name="userfile" type="file">
  <input type="submit" value="Send File">
</form>';
}
