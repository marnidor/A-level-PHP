<?php

//Выведите на экран n раз фразу "Silence is golden". Число n вводит пользователь на форме.
//Если n некорректно, вывести фразу "Bad n".
/**
 * @param int $n
 * @return string
 */
function showPhraze(int $n): string
{
    if ($n <= 0) {
        return 'Bad n';
    }
    $row = '';
    for ($i = 0; $i < $n; $i++) {
        $row .= 'Silence is golden' . "</br>";
    }
    return $row;
}
echo showPhraze($_POST['count']);
