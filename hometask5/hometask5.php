<?php

function dd($arr)
{
    echo "<pre>";
    print_r($arr);
    echo "</pre>";
}

$a = [123, 23, 45, 56];
dd($a);
echo "<hr>";


//Вам нужно создать массив и заполнить его случайными числами от 1 до 100 (ф-я rand).
//Далее, вычислить произведение тех элементов, которые больше нуля и у которых четные индексы.
//После вывести на экран элементы, которые больше нуля и у которых нечетный индекс.
/**
 * @return array
 */
function createArray(): array
{
    for ($i = 0; $i < 100; $i++) {
        $arr[] = rand(1, 100);
    }
    return $arr;
}

$arr1 = createArray();
print_r($arr1);
echo "</br>";
/**
 * @param array $arr
 * @return int
 */
function culcMultiplication(array $arr): int
{
    $mul = 1;
    for ($i = 0; $i < 10; $i++) {
        if ($arr[$i] > 0 && $i % 2 == 0) {
            $mul *= $arr[$i];
        }
    }
    return $mul;
}

echo culcMultiplication($arr1);
/**
 * @param array $arr
 * @return array
 */
function showArray(array $arr): array
{
    for ($i = 0; $i < 10; $i++) {
        if ($arr[$i] > 0 && $i % 2 == 1) {
            $array[] = $arr[$i];
        }
    }
    return $array;
}

dd(showArray($arr1));
echo "<hr>";

//Даны два числа. Найти их сумму и произведение. Даны два числа.
//Найдите сумму их квадратов.
$num1 = rand(1, 100);
$num2 = rand(1, 100);
echo $num1 . ' ' . $num2 . "</br>";
/**
 * @param int $num1
 * @param int $num2
 * @return int
 */
function calcSum1(int $num1, int $num2): int
{
    return $num1 + $num2;
}

/**
 * @param int $num1
 * @param int $num2
 * @return int
 */
function calcMultipliction1(int $num1, int $num2): int
{
    return $num1 * $num2;
}

/**
 * @param int $num1
 * @param int $num2
 * @return int
 */
function calcSquares(int $num1, int $num2): int
{
    return $num1 ** 2 + $num2 ** 2;
}

echo calcSum1($num1, $num2) . "</br>";
echo calcMultipliction1($num1, $num2) . "</br>";
echo calcSquares($num1, $num2) . "</br>";
echo "<hr>";

//Даны три числа. Найдите их среднее арифметическое.
$num1 = rand(1, 100);
$num2 = rand(1, 100);
$num3 = rand(1, 100);
echo $num1 . ' ' . $num2 . ' ' . $num3 . "</br>";
/**
 * @param int $num1
 * @param int $num2
 * @param int $num3
 * @return float
 */
function calcAvarege(int $num1, int $num2, int $num3) : float
{
    return ($num1 + $num2 + $num3) / 3;
}

echo 'Average = ' . calcAvarege($num1, $num2, $num3);
echo "<hr>";

//Дано число. Увеличьте его на 30%, на 120%.
$num = rand(1, 100);
echo $num . "</br>";
/**
 * @param int $num
 * @return int
 */
function calcPercent1(int $num): int
{
    return $num + $num * 0.3;
}

/**
 * @param int $num
 * @return int
 */
function calcPercent2(int $num): int
{
    return $num + $num * 1.2;
}

echo '+30% = ' . calcPercent1($num) . "</br>";
echo '+120% = ' . calcPercent2($num) . "</br>";
echo "<hr>";


//Найти площадь
$len = rand(1, 100);
$wid = rand(1, 100);
echo $len . ' ' . $wid . "</br>";
/**
 * @param int $len
 * @param int $wid
 * @return int
 */
function calcArea(int $len, int $wid): int
{
    return $len * $wid;
}

echo 'Area = ' . calcArea($len, $wid) . "</br>";
echo "<hr>";

//Теорема Пифагора
$len1 = 5;
$len2 = 12;
echo $len1 . ' ' . $len2 . "</br>";
/**
 * @param int $len1
 * @param int $len2
 * @return int
 */
function calcPythagor(int $len1, int $len2): int
{
    return sqrt($len1 ** 2 + $len2 ** 2);
}

echo 'Hypotenuse = ' . calcPythagor($len1, $len2) . "</br>";
echo "<hr>";

//Найти периметр
$len1 = rand(1, 100);
$len2 = rand(1, 100);
$len3 = rand(1, 100);
echo $len1 . ' ' . $len2 . ' ' . $len3 . "</br>";
/**
 * @param int $len1
 * @param int $len2
 * @param int $len3
 * @return int
 */
function calcPerimetr(int $len1, int $len2, int $len3): int
{
    return $len1 + $len2 + $len3;
}

echo 'Perimetr = ' . calcPerimetr($len1, $len2, $len3) . "</br>";
echo "<hr>";

//Найти дискриминант
$a = rand(1, 10);
$b = rand(1, 10);
$c = rand(1, 10);
echo $a . ' ' . $b . ' ' . $c . "</br>";
/**
 * @param int $a
 * @param int $b
 * @param int $c
 * @return int
 */
function calcDiscriminant(int $a, int $b, int $c): int
{
    return $b ** 2 - 4 * $a * $c;
}

echo 'Discriminant = ' . calcDiscriminant($a, $b, $c) . "</br>";
echo "<hr>";

//Создать только четные числа до 100
/**
 * @param array $arr
 * @return array
 */
function createArray1(array $arr): array
{
    for ($i = 1; $i <= 100; $i++) {
        $arr[] = $i;
    }
    return $arr;
}

$arr2 = createArray1($arr);
/**
 * @param array $arr
 * @return array
 */
function calcEven(array $arr): array
{
    for ($i = 0; $i < 100; $i++) {
        if ($arr[$i] % 2 == 0) {
            echo $arr[$i] . ' ';
        }
    }
    return $arr;
}

echo calcEven($arr2);
echo "<hr>";

//Создать не четные числа до 100
/**
 * @param array $arr
 * @return array
 */
function createArray2(array $arr): array
{
    for ($i = 1; $i <= 100; $i++) {
        $arr[] = $i;
    }
    return $arr;
}

$arr3 = createArray2($arr);

/**
 * @param array $arr
 * @return array
 */
function calcOdd(array $arr): array
{
    for ($i = 0; $i < 100; $i++) {
        if ($arr[$i] % 2 == 1) {
            echo $arr[$i] . ' ';
        }
    }
    return $arr;
}

echo calcOdd($arr3);
echo "<hr>";

//Найти минимальное и максимальное среди 3 чисел
$num1 = rand(1, 10);
$num2 = rand(1, 10);
$num3 = rand(1, 10);
echo $num1 . ' ' . $num2 . ' ' . $num3 . "</br>";
/**
 * @param int $num1
 * @param int $num2
 * @param int $num3
 * @return int
 */
function calcMin(int $num1, int $num2, int $num3): int
{
    $arr = [$num1, $num2, $num3];
    $min = $arr[0];
    for ($i = 0; $i <= 2; $i++) {
        if ($arr[$i] <= $min) {
            $min = $arr[$i];
        }
    }
    return $min;
}

echo calcMin($num1, $num2, $num3);
echo "<hr>";

//Определите, есть ли в массиве повторяющиеся элементы.
$array1 = [1, 2, 4, 6, 23, 56, 1, 8, 23];
$array2 = [1, 2, 4, 6, 23, 56, 8, 9, 10];
$count = count($array1);
/**
 * @param array $arr
 * @param int $count
 * @return int
 */
function isRepeate(array $arr, int $count):int
{
    $is = 0;
    for ($i = 0; $i < $count; $i++) {
        for ($j = 0; $j < $count; $j++) {
            if ($arr[$i] == $arr[$j] && $i != $j) {
                $is = 1;
                break;
            } else {
                $is = 0;
            }
        }
    }
    return $is;
}

echo isRepeate($array1, $count) . "</br>";
echo isRepeate($array2, $count) . "</br>";
echo "<hr>";

//Заполнить массив длины n нулями и единицами,
//при этом данные значения чередуются, начиная с нуля.
$n = 100;
/**
 * @param int $value
 * @return array
 */
function createNull(int $value): array
{
    $arr = [];
    for ($i = 0; $i < $value; $i++) {
        if ($i % 2 == 0) {
            $arr[$i] = 0;
        } else {
            $arr[$i] = 1;
        }
    }
    return $arr;
}

dd(createNull($n));
echo "<hr>";

//Выведите на экран n раз фразу "Silence is golden". Число n вводит пользователь на форме.
//Если n некорректно, вывести фразу "Bad n".
/**
 * @param int $n
 */
function showPhraze(int $n): void
{
    for ($i = 0; $i < $n; $i++) {
        echo 'Silence is golden' . "</br>";
    }
}

/**
 * @param int $num
 * @param int $deg
 * @return int
 */
//Создать функцию по нахождению числа в степени
function calcDegree(int $num, int $deg): int
{
    return $num ** $deg;
}

$num = rand(1, 10);
$deg = rand(1, 10);
echo $num . ' ' . $deg . "</br>";
echo 'Число в степени = ' . calcDegree($num, $deg);
echo "<hr>";

//написать функцию сортировки. Функция принимает массив случайных чисел и сортирует их по порядку.
//По дефолту функция сортирует в порядке возрастания. Но если передать в сторой параметр то
// функция будет сортировать по убыванию.
//sort(arr)
//sort(arr, 'asc')
//sort(arr, 'desc')
$sortArr = createArray();
dd($sortArr);
/**
 * @param array $arr
 * @param string $sortType
 * @return array
 */
function sortArray(array $arr, string $sortType = 'asc'): array
{
    $len = count($arr);
    if ($sortType !== 'asc' || $sortType !== 'desc') {
        $sortType = 'asc';
    }
    for ($i = 0; $i < $len; $i++) {
        for ($j = 0; $j < $len - 1; $j++) {
            if (
                ($arr[$j] >= $arr[$j + 1] && $sortType == 'asc') ||
                ($arr[$j] <= $arr[$j + 1] && $sortType == 'desc')
            ) {
                $temp = $arr[$j];
                $arr[$j] = $arr[$j + 1];
                $arr[$j + 1] = $temp;
            }
        }
    }
    return $arr;
}

$arr1 = sortArray($sortArr, 'asc');
dd($arr1);
echo "<hr>";


//написать функцию поиска в массиве. функция будет принимать два параметра.
// Первый массив, второй поисковое число. search(arr, find)
$searchArr = createArray();
dd($searchArr);
$find = rand(1, 100);
/**
 * @param array $arr
 * @param int $find
 */

function searchElement(array $arr, int $find): void
{
    $len = count($arr);
    for ($i = 0; $i < $len; $i++) {
        if ($arr[$i] == $find) {
            echo "Найдено число - $find " . "на $i месте";
            return;
        }
    }
    echo 'Нет нужного числа в массиве';
}

echo searchElement($searchArr, $find);
