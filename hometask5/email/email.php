<?php

//Пользователь вводит свой имя, пароль, email. Если вся информация указана,
//то показать эти данные после фразы 'Регистрация прошла успешно',
// иначе сообщить какое из полей оказалось не заполненным.
$name = isset($_POST['name']) ? $_POST['name'] : false;
$password = isset($_POST['password']) ? $_POST['password'] : false;
$email = isset($_POST['email']) ? $_POST['email'] : false;

/**
 * @param string $name
 * @param string $password
 * @param string $email
 */
function showData(string $name, string $password, string $email): void
{
    if (isset($name) && isset($password) && isset($email)) {
        echo 'Регистрация прошла успешно' . "name = $name " . "password = $password " . "email = $email " . "</br>";
    }
}

/**
 * @param string $name
 * @return bool
 */
function isValidName(string $name): bool
{
    if (!$name) {
        echo 'Поле Name не заполнено' . "</br>";
        return false;
    }
    return true;
}

/**
 * @param string $password
 * @return bool
 */
function isValidPassword(string $password): bool
{
    if (!$password) {
        echo 'Поле Password не заполнено' . "</br>";
        return false;
    }
    return true;
}

function isValidEmail($email)
{
    if (!$email) {
        echo 'Поле Email не заполнено' . "</br>";
        return false;
    }
    return true;
}

if (isValidPassword($_POST['password']) && isValidName($_POST['name']) && isValidEmail($_POST['email'])) {
    showData($_POST['name'], $_POST['password'], $_POST['email']);
}
