<?php

//Пользователь выбирает из выпадающего списка страну (Турция, Египет или Италия),
//вводит количество дней для отдыха и указывает, есть ли у него скидка (чекбокс).
//Вывести стоимость отдыха, которая вычисляется как произведение количества дней на 400.
//Далее это число увеличивается на 10%, если выбран Египет, и на 12%, если выбрана Италия.
//И далее это число уменьшается на 5%, если указана скидка.

/**
 * @param int $days
 * @param int $country
 * @param null $discount
 * @return float
 */
function calcCost(int $days, int $country, $discount = null): float
{
    $cost = $days * 400;
    if ($country == 'Италия') {
        $cost *= 1.12;
    } elseif ($country == 'Египет') {
        $cost *= 1.1;
    }
    if ($discount) {
        $cost *= 0.95;
    }
    return $cost;
}

$discount = isset($_POST['hasDiscount']) ? true : false;
echo calcCost($_POST['days'], $_POST['country'], $discount);
