<?php

class Figure
{
    public $type, $color, $square;

    public function set_type ($type) {
        $this->type = $type;
        return $this;
    }

    public function set_color ($color) {
        $this->color = $color;
        return $this;
    }

    public function set_square ($square) {
        $this->square = $square;
        return $this;
    }

    public function get_type () {
        return $this;
    }

    public function get_color () {
        return $this;
    }

    public function get_square () {
        return $this;
    }

    public function get_info() {
        return 'Type:' . $this->type . "<br/>" . 'Color: ' . $this->color . "<br/>" . 'Square: ' . $this->square . "<br/>";
    }
}

class Triangle extends Figure {
    protected $amountSides;

    public function set_amountSides ($amountSides) {
        $this->amountSides = $amountSides;
        return $this;
    }

    public function get_amountSides () {
        return $this;
    }

    public function get_info(){
        return parent::get_info(). " Amout sides: " . $this->amountSides . "<br/>";

    }
}

class RightTriangle extends Triangle {
    protected $angle;

    public function set_angle ($angle) {
        $this->angle = $angle;
        return $this;
    }

    public function get_angle () {
        return $this;
    }

    public function get_info(){
        return parent::get_info(). " Angles: " . $this->angle . "<br/>";

    }
}

$triangle = new RightTriangle();
$triangle
    ->set_type("Triangle")
    ->set_color("Red")
    ->set_square("20")
    ->set_amountSides("3")
    ->set_angle("90 degrees, 30 degrees, 60 degrees");

echo $triangle->get_info();
